# Write flow code that fetches other weather metrics
# Make at least 3 tasks in the flow
# Example: windspeed for the last hour:
# weather.json()[“hourly”][“windspeed_10m”][0]
# Docs: open-meteo.com/en/docs

import httpx
import random
from datetime import timedelta
from prefect import flow,  task, get_run_logger
from prefect.tasks import task_input_hash


@task(cache_key_fn=task_input_hash, cache_expiration=timedelta(hours=1))
def get_api_url(logger):
    url = "https://api.open-meteo.com/v1/forecast"
    logger.debug(f"API url is: {url}")
    return url

@task(
    retries=2, 
    retry_delay_seconds=1,
    cache_key_fn=task_input_hash, 
    cache_expiration=timedelta(hours=1)
)
def get_api_request(logger, api_url, params):
    random_num = random.randint(1, 4)
    logger.info(f"Random number (for failures simulation): {random_num}")
    if random_num == 1:
        logger.debug("Going to request to the API...")
        res = httpx.request(
            method="get",
            url=api_url,
            params=params
        )
        return res
    else:
        err_msg = "Simulation of failure"
        logger.error(err_msg)
        raise Exception(err_msg)

@task(retries=5, retry_delay_seconds=1)
def parse_result(logger, res):
    logger.debug(f"API result: {res}")
    if res.status_code == 200:
        logger.info("Res code 200 OK")
        return res.json()    
    else:
        logger.error(f"Res code isn't 200 - we got {res.status_code}")
        raise Exception("API returned wrong result")
    
@flow(
    retries=4, 
    retry_delay_seconds=5,
    name="lab105-main-flow"
)
def get_recent_windspeed_10m(lat=33.7254, lon=112.2672):
    logger = get_run_logger()
    logger.info("Running get_api_url")
    api_url = get_api_url(logger)    
    logger.info("Running get_api_request")
    res = get_api_request(logger, api_url , dict(latitude=lat, longitude=lon, hourly="windspeed_10m"))
    logger.info("Running parse_result")
    res_json = parse_result(logger, res)
    return res_json["hourly"]["windspeed_10m"][-1]

@flow(
    retries=4,
    retry_delay_seconds=10,
    name="lab105-subflow"
)
def  a_random_subflow(random_parameter="XXX"):
    return f"Just a random result with parameter {random_parameter}"

@flow(log_prints=True)
def run_2_flows():
    miami = {"lat": 25.761681, "lon": -80.191788}
    miami_windspeed = get_recent_windspeed_10m(**miami)
    random_string = a_random_subflow()
    print(f"{random_string} ...By the way, most recent windspeed in Miami {miami_windspeed}")

if __name__ == "__main__":
    run_2_flows()